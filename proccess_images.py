import os
from PIL import Image
import dao as Dao
import hsv_helper as HSVHelper

def process_image(path):
    image = Image.open(path)
    if (image.mode == 'L'): # Skip black and white images
        return
    image_hsv = HSVHelper.convert_to_hsv(image)
    imageData, new_im = HSVHelper.extract_colors(image_hsv, path)
    Dao.save_image_data(imageData)

def process_images():
    k=1
    for (dir, allDirs, files) in walk('images'):
        if (k>30): # Load only k folders from images
            break
        print("importing images from " + dir)
        for file in files:
            path = dir + '/' + file
            process_image(path)
        k+=1

# Create a database of image features
if os.path.isdir("images"):
    process_images()
else:
    print("There are no images to work with. Download and store images to images folder")