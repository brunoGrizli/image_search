import numpy as numpy

def init_colors():
    return {
        "red": {
            "count": 0,
            "ro": 0,
            "value": 0
        },
        "orange": {
            "count": 0,
            "ro": 0,
            "value": 0
        },
        "yellow": {
            "count": 0,
            "ro": 0,
            "value": 0
        },
        "green": {
            "count": 0,
            "ro": 0,
            "value": 0
        },
        "blue": {
            "count": 0,
            "ro": 0,
            "value": 0
        },
        "purple": {
            "count": 0,
            "ro": 0,
            "value": 0
        },
        "shade": {
            "count": 0,
            "ro": 0,
            "value": 0
        }
    };

def init_data(imagePath):
    image_data = {}
    image_data["path"] = imagePath
    image_data["all"] = {
        "colors": init_colors(),
        "histogram": [],
        "valueHistogram": []
    }
    image_data["top"] = {
        "colors": init_colors(),
        "histogram": [],
        "valueHistogram": []
    }
    image_data["middle"] = {
        "colors": init_colors(),
        "histogram": [],
        "valueHistogram": []
    }
    image_data["bottom"] = {
        "colors": init_colors(),
        "histogram": [],
        "valueHistogram": []
    }
    return image_data;

def aggregate_image_data(image_part_dict, total_pixels):
    part_colors = image_part_dict["colors"]
    for color in part_colors:
        ro = part_colors[color]["count"] / total_pixels
        val = part_colors[color]["value"] / total_pixels
        part_colors[color]["ro"] = ro
        part_colors[color]["value"] = val
        image_part_dict["histogram"].append(ro)
        image_part_dict["valueHistogram"].append(val)
    return image_part_dict

def extract_colors(image, imagePath):
    image_data = init_data(imagePath);
    image_ar = numpy.array(image)
    total_pixels = image_ar.shape[0] * image_ar.shape[1]
    total_rows = image_ar.shape[0]
    r = 0
    new_im = []
    for row in image_ar:
        new_im.append([])

        if (r == 0 or r < (total_rows / 3)):
            image_part = "top"
        elif (r < (2 * total_rows / 3)):
            image_part = "middle"
        else:
            image_part = "bottom"

        for col in row:
            hsv = col
            hue_degrees = (hsv[0]/255)*360
            value = (hsv[2]/255)
            saturation = (hsv[1]/255)
            current_pixel_color = "shade"
            if (value < 0.2 or saturation < 0.2):
                current_pixel_color = "shade"
                new_im[r].append([255, 170, 0])
            elif hue_degrees >= 320 or hue_degrees <= 20:
                current_pixel_color = "red"
                new_im[r].append([255, 255, 255])
            elif hue_degrees > 20 and hue_degrees <= 40:
                current_pixel_color = "orange"
                new_im[r].append([20, 255, 255])
            elif hue_degrees > 40 and hue_degrees <= 70:
                current_pixel_color = "yellow"
                new_im[r].append([40, 255, 255])
            elif hue_degrees > 70 and hue_degrees <= 160:
                current_pixel_color = "green"
                new_im[r].append([85, 255, 255])
            elif hue_degrees > 160 and hue_degrees <= 260:
                current_pixel_color = "blue"
                new_im[r].append([150, 255, 255])
            elif hue_degrees > 260 and hue_degrees <= 320:
                current_pixel_color = "purple"
                new_im[r].append([205, 255, 255])

            image_data["all"]["colors"][current_pixel_color]["count"] += 1
            image_data["all"]["colors"][current_pixel_color]["value"] += value + saturation
            image_data[image_part]["colors"][current_pixel_color]["count"] += 1
            image_data[image_part]["colors"][current_pixel_color]["value"] += value + saturation
        r += 1

    aggregate_image_data(image_data["all"], total_pixels)
    aggregate_image_data(image_data["top"], total_pixels)
    aggregate_image_data(image_data["middle"], total_pixels)
    aggregate_image_data(image_data["bottom"], total_pixels)

    new_im = numpy.array(new_im)
    new_im = new_im.astype(numpy.uint8)
    return image_data, new_im

def convert_to_hsv(image):
    return image.convert('HSV')

def get_hues(image):
    hsv_image = convert_to_hsv(image)
    image_array = numpy.array(hsv_image)
    return image_array[0:, 0:, 0].flatten()

def get_values_histogram(colors):
    histogram = []
    for color in colors:
        histogram.append(colors[color]["value"])
    return histogram