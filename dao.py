import pymongo
from pymongo import MongoClient

mongoClient = MongoClient()
db = mongoClient.imageAnalytics

def save_image_data(imageData):
    savedImageData = db.colorHistograms.find_one({"path": imageData["path"]})
    if (savedImageData is None):
        db.colorHistograms.insert_one(imageData)
    else:
        db.colorHistograms.update(savedImageData, imageData)

def get_all_images_data():
    return db["colorHistograms"].find()
