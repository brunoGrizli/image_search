# Project setup

1. Download images from http://www.vision.caltech.edu/Image_Datasets/Caltech101/
2. Store images to images folder
3. Install python dependencies:
    a. py -m pip install PIL
    b. py -m pip install matplotlib
    c. py -m pip install numpy
    d. py -m pip install pymongo

