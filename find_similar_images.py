from os import walk
from PIL import Image
import dao as Dao
import hsv_helper as HSVHelper
import operator
import histogram_functions as HistogramFunctions
from matplotlib.pyplot import *
from pathlib import Path

def load_images():
    cursor = Dao.get_all_images_data()
    if cursor.count() == 0:
        print("There are no images in the databse. Run process_images.py to load image fetures from images folder.")
        return
    images = []
    images.extend(cursor[:])
    return images

# find nearest neighbours
def find_similar_images(imagePath):
    images = load_images()
    image = Image.open(imagePath)
    if (image.mode == 'L'):
        print("Cant find matches for grayscale images")
        return
    image_hsv = HSVHelper.convert_to_hsv(image)
    image_data, new_im = HSVHelper.extract_colors(image_hsv, imagePath)

    print("Image colors")
    print(image_data)

    dist = {}
    for im in images:
        distance = 0;

        distance += HistogramFunctions.distance_chisqrt(im["top"]["histogram"], image_data["top"]["histogram"])
        distance += HistogramFunctions.distance_chisqrt(im["top"]["valueHistogram"], image_data["top"]["valueHistogram"])

        distance += HistogramFunctions.distance_chisqrt(im["middle"]["histogram"], image_data["middle"]["histogram"])
        distance += HistogramFunctions.distance_chisqrt(im["middle"]["valueHistogram"], image_data["middle"]["valueHistogram"])

        distance += HistogramFunctions.distance_chisqrt(im["bottom"]["histogram"], image_data["bottom"]["histogram"])
        distance += HistogramFunctions.distance_chisqrt(im["middle"]["valueHistogram"], image_data["middle"]["valueHistogram"])
        dist[im["path"]] = distance

    # sort
    distSorted = sorted(dist.items(), key=operator.itemgetter(1))
    return distSorted[0:5], image

# Display results
def showResults(nearestNeighbours, image):
    print(nearestNeighbours)
    nmbr = (len(nearestNeighbours) + 1) # +1 for origin image, *2 for more space
    k = 0
    fig = figure(1)

    for neighbour in nearestNeighbours:
        imagePath = neighbour[0]
        p = Path(imagePath)
        im = Image.open(p)
        fig.add_subplot(nmbr, 2, k+1).imshow(im)
        fig.add_subplot(nmbr, 2, k+2).hist(HSVHelper.get_hues(im), 255)
        k += 2

    #finaly add origin image
    fig.add_subplot(nmbr, 2, k+1).imshow(image)
    fig.add_subplot(nmbr, 2, k+2).hist(HSVHelper.get_hues(image), 255)
    show()
    return

similar_dict, image = find_similar_images("images/butterfly/image_0025.jpg")
print(similar_dict)
showResults(similar_dict, image)


